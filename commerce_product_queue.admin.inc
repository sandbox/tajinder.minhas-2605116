<?php

/**
 * @file
 * Admin related functionlity.
 */

/**
 * Confirm form to delete a queue.
 */
function commerce_product_queue_admin_delete($form, &$form_state, $queue) {
  $form['pqid'] = array('#type' => 'value', '#value' => $queue->pqid);
  return confirm_form($form, t('Are you sure you want to delete "%title"?', array('%title' => $queue->title)), isset($_GET['destination']) ? $_GET['destination'] : 'admin/structure/commerce-product-queue', t('This action cannot be undone.'), t('Delete'), t('Cancel')
  );
}

/**
 * Submit function for product queue delete.
 */
function commerce_product_queue_admin_delete_submit($formid, &$form_state) {
  if ($form_state['values']['confirm']) {
    commerce_product_queue_delete_queue($form_state['values']['pqid']);
  }
  $form_state['redirect'] = 'admin/structure/commerce-product-queue';
}

/**
 * Page callback for the main admin page.
 */
function commerce_product_queue_list_queues() {
  $queues = commerce_product_queue_load_queues();
  if (empty($queues)) {
    return t('No product queues exist.');
  }

  $header = array(
    array('data' => t('Title'), 'field' => 'title', 'sort' => 'asc'),
    array('data' => t('Max Productss'), 'field' => 'size'),
    array('data' => t('Operation')),
  );
  $table_sort = tablesort_init($header);

  $sort_primary = array();
  $sort_secondary = array();
  $sort_direction_regular = array('asc' => SORT_ASC, 'desc' => SORT_DESC);
  $sort_direction_reverse = array('asc' => SORT_DESC, 'desc' => SORT_ASC);
  foreach ($queues as $queue) {
    $sort_secondary[] = drupal_strtolower($queue->title);
    switch ($table_sort['sql']) {
      case 'title':
      default:
        $sort_primary[] = drupal_strtolower($queue->title);
        $sort_direction = $sort_direction_regular;
        break;

      case 'size':
        $sort_primary[] = $queue->size;
        $sort_direction = $sort_direction_reverse;
        break;
    }
  }

  if (!empty($table_sort)) {
    if (strtolower($table_sort['sort']) == 'desc') {
      // Re-indexes array keys; key no longer equals qid.
      array_multisort($sort_primary, $sort_direction['desc'], $sort_secondary, $queues);
    }
    else {
      // Re-indexes array keys; key no longer equals qid.
      array_multisort($sort_primary, $sort_direction['asc'], $sort_secondary, $queues);
    }
  }

  $rows = array();
  foreach ($queues as $queue) {
    $operations = array(l(t('View'), "admin/structure/commerce-product-queue/$queue->pqid/view"));
    if (user_access('administer commerce product queues')) {
      $operations[] = l(t('Edit'), "admin/structure/commerce-product-queue/$queue->pqid/edit");
      $operations[] = l(t('Delete'), "admin/structure/commerce-product-queue/$queue->pqid/delete");
    }

    $rows[] = array(
      array(
        'class' => array('commerce-product-queue-title'),
        'data' => check_plain($queue->title),
      ),
      array(
        'class' => array('commerce-product-queue--max-products'),
        'data' => $queue->size == 0 ? t('Infinite') : $queue->size,
      ),
      array(
        'class' => array('commerce-product-queue-operation'),
        'data' => implode(' | ', $operations),
      ),
    );
  }
  $output = '';
  $output .= theme('table', array('header' => $header, 'rows' => $rows));
  $output .= theme('pager', array('tags' => NULL));
  return $output;
}

/**
 * Machinery for editing queue metadata (not adding/ordering/removing products).
 */
function commerce_product_queue_edit_form($form, &$form_state, $queue) {
  $form = array();
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Queue title'),
    '#default_value' => !empty($queue->title) ? check_plain($queue->title) : '',
    '#required' => TRUE,
  );
  $form['size'] = array(
    '#type' => 'textfield',
    '#title' => t('Queue length'),
    '#size' => 5,
    '#default_value' => !empty($queue->size) ? check_plain($queue->size) : '',
    '#description' => t('Maximum number of products allowed in the queue'),
    '#element_validate' => array('element_validate_integer_positive'),
  );
  $form['reverse'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show items in reverse'),
    '#default_value' => !empty($queue->reverse) ? check_plain($queue->reverse) : '',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  if (!empty($queue->pqid)) {
    $form['pqid'] = array(
      '#type' => 'hidden',
      '#value' => $queue->pqid,
    );
    $form['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
    );
  }
  return $form;
}

/**
 * Submit function for the queue edit form.
 */
function commerce_product_queue_edit_form_submit($form, &$form_state) {
  $val = $form_state['values'];
  if ($val['op'] == t('Delete')) {
    // Redirecting to confirmation form.
    drupal_goto('admin/structure/commerce-product-queue/' . $val['pqid'] . '/delete');
  }

  if (!empty($val['pqid'])) {
    // Update existing queue.
    commerce_product_queue_update_queue($val, $val['pqid']);
    drupal_goto('admin/structure/commerce-product-queue');
  }
  else {
    // Insert a new product queue.
    commerce_product_queue_create_queue($val);
    drupal_goto('admin/structure/commerce-product-queue');
  }
}
