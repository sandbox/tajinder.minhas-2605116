<?php

/**
 * @file
 * Menu callback, arrange product queue form.
 */

/**
 * View function for a queue.
 */
function commerce_product_queue_view_queue($queue) {
  $products = commerce_product_queue_get_products($queue);
  return drupal_get_form('commerce_product_queue_arrange_subqueue_form', $queue, $products);
}

/**
 * Menu callback, remove product from a product queue.
 */
function commerce_product_queue_admin_remove_product($queue, $product) {
  db_delete('commerce_product_queue_product')
      ->condition('pid', $product->product_id)
      ->condition('pqid', $queue->pqid)
      ->execute();
  drupal_set_message(t('Product deleted from queue successfully.'));
  drupal_goto($_GET['destination']);
}

/**
 * Form definition for product queue drag'n'drop form.
 */
function commerce_product_queue_arrange_subqueue_form($form, &$form_state, $queue, $products) {
  $form = array('#tree' => TRUE);
  // Prepare the main part of the form which will be themed as a table.
  $count = count($products);
  $form['products'] = array();
  $form['products']['#theme'] = 'commerce_product_queue_arrange_subqueue_form_table';
  // Theme function needs these.
  $form['products']['#queue'] = (array) $queue;
  foreach ($products as $product) {
    $form['products'][$product->product_id]['#product'] = (array) $product;
    $form['products'][$product->product_id]['title'] = array('#markup' => $product->title);
    $form['products'][$product->product_id]['date'] = array(
      '#markup' => date('F j, y, g:i a', $product->created),
    );
    $form['products'][$product->product_id]['edit'] = array(
      '#markup' => l(t('edit'), 'admin/commerce/products/' . $product->product_id . '/edit',
      array(
        'query' => array('destination' => 'admin/structure/commerce-product-queue/' . $queue->pqid . '/view'),
      )),
    );

    $form['products'][$product->product_id]['queue_id'] = array(
      '#type' => 'hidden',
      '#value' => $queue->pqid,
    );
    $form['products'][$product->product_id]['weight'] = array(
      '#type' => 'weight',
      '#title' => t('Weight'),
      '#default_value' => $product->position,
      '#title_display' => 'invisible',
      '#attributes' => array(
        'class' => array('commerce-product-queue-table-drag'),
      ),
    );

    $attr = array(
      '#attributes' => array(
        'title' => t('Remove from queue'),
        'style' => 'display: none;',
        'class' => array('commerce-product-queue-remove'),
        'id' => 'commerce-product-queue-remove-' . $product->product_id,
      ),
      'query' => drupal_get_destination(),
    );
    $form['products'][$product->product_id]['remove'] = array(
      '#markup' => l(t('remove'), 'commerce_product_queue/' . $queue->pqid . '/remove-product/' . $product->product_id, $attr),
    );
  }

  // Textfield for adding products to the queue.
  $form['add'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('container-inline')),
  );
  $form['add']['product'] = array(
    '#type' => 'textfield',
    '#autocomplete_path' => 'commerce_product/queue/autocomplete',
    '#maxlength' => 1024,
  );
  $form['add']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add product'),
    '#submit' => array('commerce_product_queue_arrange_subqueue_form_add_submit'),
  );
  // Submit, reverse, shuffle, and clear actions.
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  // Disable all buttons if the queue is empty.
  if ($count == 0) {
    $form['actions']['submit']['#disabled'] = TRUE;
  }

  return $form;
}

/**
 * Submit handler, add a new product in the queue.
 */
function commerce_product_queue_arrange_subqueue_form_add_submit($form, &$form_state) {
  $val = $form_state['values']['add'];
  $pid = $val['product'];
  $queue_products = array_keys($form_state['values']['products']);
  if (in_array($pid, $queue_products)) {
    drupal_set_message(t('Added product is already in Queue'), 'error');
  }
  else {
    $pqid = $form['products']['#queue']['pqid'];
    $query = db_select('commerce_product_queue_product', 'dp');
    $query->addExpression('MAX(position)', 'position');
    $position = $query->execute()->fetchField();
    if ($pid) {
      $item = new stdClass();
      $item->pid = $pid;
      $item->position = ($position + 1);
      $item->pqid = $pqid;
      drupal_write_record('commerce_product_queue_product', $item);
      drupal_set_message(t('Product %productname has been added', array('%productname' => $val['product'])));
    }
  }
}

/**
 * Submit handler for arranging products.
 */
function commerce_product_queue_arrange_subqueue_form_submit($form, &$form_state) {
  foreach ($form_state['values']['products'] as $pid => $element) {
    db_update('commerce_product_queue_product')
        ->fields(array(
          'position' => $element['weight'],
        ))
        ->condition('pid', $pid)
        ->condition('pqid', $element['queue_id'])
        ->execute();
  }
}
