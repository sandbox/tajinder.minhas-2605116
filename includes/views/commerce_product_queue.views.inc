<?php

/**
 * @file
 * Provides position sort filter for CPQ.
 */

/**
 * Implements hook_views_data().
 */
function commerce_product_queue_views_data() {
  $data = array();

  $data['commerce_product_queue_product']['table']['group'] = t('Commerce Product');
  $data['commerce_product_queue_product']['table']['join'] = array(
    'commerce_product' => array(
      'left_field' => 'product_id',
      'field' => 'pid',
    ),
  );

  $data['commerce_product_queue_product']['position'] = array(
    'title' => t('Position'),
    'help' => t('Position of the product in the given queue.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  return $data;
}

/**
 * Implements hook_views_data_alter().
 */
function commerce_product_queue_views_data_alter(&$data) {
  $data['commerce_product_queue_product']['product_queue_rel'] = array(
    'title' => t('Product Queue'),
    'relationship' => array(
      'base' => 'commerce_product',
      'base field' => 'product_id',
      'field' => 'pid',
      'handler' => 'CommerceProductQueueRelationship',
      'label' => t('Default label for relationship'),
      'title' => t('Product Queue'),
      'help' => t('Add relationship with commerce product queue.'),
    ),
  );
}
