<?php

/**
 * @file
 * Provides relarionship handler for CPQ.
 */

/**
 * Extends Relationship handler.
 */
class CommerceProductQueueRelationship extends views_handler_relationship {

  /**
   * {@inheritdoc}
   */
  public function option_definition() {
    $options = parent::option_definition();

    $options['product_queue']['default'] = FALSE;
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $product_queues = commerce_product_queue_load_queues();
    $queue = array();
    foreach ($product_queues as $key => $val) {
      $queue[$key] = check_plain($val->title);
    }
    $form['product_queue'] = array(
      '#type' => 'select',
      '#options' => array('' => t('-- Select --')) + $queue,
      '#title' => t('Select product queue'),
      '#default_value' => $this->options['product_queue'],
      '#required' => TRUE,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $table_name = 'commerce_product_queue_product';
    $column_name = 'pqid';
    $queue_id = $this->options['product_queue'];
    $join = new views_join();
    $join->table = $table_name;
    $join->left_table = 'commerce_product';
    $join->field = 'pid';
    $join->left_field = 'product_id';
    $join->type = 'LEFT';
    $alias = $this->table;
    $this->query->add_relationship($alias, $join, $this->definition['base'], $this->relationship);
    if (!empty($queue_id)) {
      $this->query->add_where($this->options['group'], $table_name . '.' . $column_name, $queue_id, '=');
    }
  }

}
