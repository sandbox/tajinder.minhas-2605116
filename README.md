Commerce Product Queue will be used to get Commerce
products in a queue. User will have the ability to
adjust and change the order of products in Commerce Product
queue. This can be further used with views to set order
according to Commerce Product Queue.
One such usecase can be carousel or slideshow
where right now there is the possibility only
for node but this module you will have ability
to work with commerce product queue entity.
